var index = location.href.indexOf('=');
var id = location.href.slice(index + 1);

var main = document.getElementsByTagName('main')[0];
var button = document.querySelector('main .button');
var scraffi = document.querySelector('main img');
var note = document.querySelector('main p');

var wrapper = null;

// на моментотрисовки кнопки купить список валют уже должен быть получен поэтому 3 аргумент true
var xhrCurrency = new XMLHttpRequest();
xhrCurrency.open('GET', 'https://netology-fbb-store-api.herokuapp.com/currency/', true);
xhrCurrency.send();

xhrCurrency.onload = function () {
    sessionStorage.currency = xhrCurrency.responseText;
};


var xhr = new XMLHttpRequest();
xhr.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book/' + id);
xhr.send();

xhr.onload = function () {
    sessionStorage.bookDetails = xhr.responseText;
    var res = JSON.parse(sessionStorage.bookDetails);

    if(xhr.status === 200) {
        showBook(res);
    }
};

function showBook(obj) {
    var div = document.createElement('div');
    div.classList.add('row');

    // Вывод обложки cover и описания книги
    var image = document.createElement('img');
    image.src = obj.cover.large;
    image.classList.add('cover');
    image.onload = showEye;
    var description = document.createElement('p');
    description.innerHTML = obj.description;
    var div1 = document.createElement('div');
    div1.classList.add('col-md-4'); // контейнер создал для целей bootstrap (для адаптивности)
    div1.classList.add('col-md-push-4');
    wrapper = document.createElement('div');
    wrapper.classList.add('wrapper'); // создал для абсолютно позиционированного глаза внутри

    wrapper.appendChild(image);
    div1.appendChild(wrapper);
    div1.appendChild(description);
    div.appendChild(div1);

    // Вывод review
    var div2 = document.createElement('div');
    div2.classList.add('col-md-4');
    div2.classList.add('col-md-pull-4');

    for(var i = 0; i < obj.reviews.length; i++) {
        var imageAuthor = document.createElement('img');
        imageAuthor.src = obj.reviews[i].author.pic;
        var cite = document.createElement('p');
        cite.innerHTML = obj.reviews[i].cite;
        cite.classList.add('cite');

        div2.appendChild(imageAuthor);
        div2.appendChild(cite);
    }
    div.appendChild(div2);

    // Вывод раздела features
    var div3 = document.createElement('div');
    div3.classList.add('col-md-4');

    for(var i = 0; i < obj.features.length; i++) {
        var imageFeatures = document.createElement('img');
        imageFeatures.src = obj.features[i].pic;
        var title = document.createElement('p');
        title.innerHTML = obj.features[i].title;

        div3.appendChild(imageFeatures);
        div3.appendChild(title);
    }
    div.appendChild(div3);
    main.appendChild(div);

    // нахождение CharCode — буквенный код
    var currency = JSON.parse(sessionStorage.currency);
    for(i = 0; i < currency.length; i++) {
        if(currency[i].ID == obj.currency) {
            button.firstElementChild.textContent = 'Купить за жалкие ' + obj.price + ' ' + currency[i].CharCode;
            button.firstElementChild.href += '?id=' + id + '&char=' + currency[i].CharCode;
            break;
        }
    }


    main.appendChild(button);
    main.appendChild(note);
    main.appendChild(scraffi);
}



function showEye() {
    var eye = document.createElement('div');
    eye.classList.add('eye');
    setTimeout(function() {
        eye.classList.add('blink');
    }, 1000);
    var apple = document.createElement('div');
    apple.classList.add('apple');

    eye.appendChild(apple);
    wrapper.appendChild(eye);

    var eyeCoord = eye.getBoundingClientRect();
    var center = { // сместил центр окружности, т.к. позиционирование элемента происходит по верхнему левому углу
        x: eyeCoord.left,
        y: eyeCoord.top
    };
    var radiusApple = apple.getBoundingClientRect().height / 2;

    document.addEventListener('mousemove', function (evt) {
        var offsetY, offsetX;
        var fullHeight, fullWidth;
        var heightMouse, widthMouse;

        // Движение по вертикали
        if((center.y - evt.clientY) > 0) {  // верхняя половина по отношению к центру
            offsetY = (100 - (evt.clientY *100 / center.y)) * eyeCoord.height / 2 /100 + radiusApple;
            if(offsetY >= eyeCoord.height / 2) {
                offsetY = eyeCoord.height / 2;
            }
            offsetY = -offsetY;

        } else {                       // нижняя половина
            fullHeight = document.documentElement.clientHeight - center.y;
            heightMouse = evt.clientY - center.y;

            offsetY = ((heightMouse *100 / fullHeight)) * eyeCoord.height / 2 /100 - radiusApple;
            if((offsetY + radiusApple * 2) >= eyeCoord.height / 2) {
                offsetY = eyeCoord.height / 2  - radiusApple * 2;
            }
        }

        // Движение по горризонтали
        if((center.x - evt.clientX) > 0) {  // левая половина
            offsetX = (100 - (evt.clientX *100 / center.x)) * eyeCoord.width / 2 /100 + radiusApple;
            if(offsetX >= eyeCoord.width / 2) {
                offsetX = eyeCoord.width / 2;
            }
            offsetX = -offsetX;
            
        } else {                          // правая половина
            fullWidth = document.documentElement.clientWidth - center.x;
            widthMouse = evt.clientX - center.x;

            offsetX = ((widthMouse *100 / fullWidth)) * eyeCoord.width / 2 /100 - radiusApple;
            if((offsetX + radiusApple * 2) >= eyeCoord.width / 2) {
                offsetX = eyeCoord.width / 2  - radiusApple * 2;
            }
        }

        // проверка, чтобы зрачки (apple) не вылазили из орбит (eye)
        // гипотенузы относятся друг к другу так же, как катеты друг к другу
        var hypotenuse = Math.sqrt(Math.pow(offsetY, 2) + Math.pow(offsetX, 2));

        if(offsetX > 0 || offsetY > 0) {
            if((hypotenuse + 10) > eyeCoord.width / 2) {
                var k = (hypotenuse+15) / (eyeCoord.width / 2);
                offsetX = offsetX / k;
                offsetY = offsetY / k;
            }
        } else {
            if(hypotenuse > eyeCoord.width / 2) {
                var k = (hypotenuse) / (eyeCoord.width / 2);
                offsetX = offsetX / k;
                offsetY = offsetY / k;
            }
        }

        apple.style.marginTop = offsetY  + 'px';
        apple.style.marginLeft = offsetX  + 'px';

        // проверка
        /*var a = evt.clientY - center.y;
         var b = evt.clientX - center.x;
         var c = apple.getBoundingClientRect().top - center.y;
         var d = apple.getBoundingClientRect().left - center.x;
         console.log(a + ' : ' + b);
         console.log(c + ' : ' + d);*/
    });
}

// Моргание blinking На 93 eye.classList.add('blink');
document.addEventListener('animationend', function() {
    var eye = $('.eye');
    eye.removeClass('blink');

    setTimeout(function() {
        eye.addClass('blink');
    }, 5000);
});