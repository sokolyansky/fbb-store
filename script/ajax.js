var page;
var res;
var main = document.getElementsByTagName('main')[0];
var button = document.querySelector('main .button');

function reinitializeVariable() {
    button = document.querySelector('main .button');
}

if(sessionStorage.booksList) {

    main.innerHTML = sessionStorage.booksList;
    delete sessionStorage.booksList;
    reinitializeVariable();  // при замене содержимого main на ранеесохраненное содержиое этого блока (при переходе по ссылке)- ссылка на button уже будет другая, т.к. это будет другой элемент
    // для этого нужно найти и получить ссылку на установленный элемент button

    button.onclick = function() {
        addBook(JSON.parse(sessionStorage.books));
    };

} else {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book');
    xhr.send();

    xhr.onload = function () {
        sessionStorage.books = xhr.responseText;
        res = JSON.parse(sessionStorage.books);

        if(xhr.status === 200) {
            addBook(res);
        }
    };

    page = sessionStorage.page = 0;

    button.onclick = function() {
        addBook(res);
    };
}

function addBook(obj) {
    page = sessionStorage.page;

    var end = page * 4 + 4 > obj.length ? obj.length : page * 4 + 4;
    if(obj.length - (page * 4 + 4) <= 0) {
        button.firstElementChild.disabled = 'true';
        button.firstElementChild.classList.add('disable');
    }

    var div = document.createElement('div');
    div.classList.add('row');

    for(var i = page * 4; i < end; i++) {
        var a = document.createElement('a');
        a.href = "book.html?id=" + obj[i].id;
        a.classList.add('col-md-3');
        var image = document.createElement('img');
        image.src = obj[i].cover.small;
        var info = document.createElement('p');
        info.innerHTML = obj[i].info;

        a.appendChild(image);
        a.appendChild(info);
        div.appendChild(a);
    }
    main.insertBefore(div, button);

    sessionStorage.page++;
}

main.addEventListener('click', function (evt) {
    var elem = evt.target;
    if (elem.closest('a')) {
        sessionStorage.booksList = main.innerHTML;
    }
});