var index = location.href.indexOf('id=');
var id = location.href.slice(index + 3, index + 11);
index = location.href.indexOf('char=');
var currentCharCode = location.href.slice(index + 5, index + 8);
var delivery = document.querySelector('.delivery');
var payment = document.querySelector('.payment');
var sendOrderButton = document.getElementsByTagName('button')[1];

var email = document.getElementById('email');
var fullName = document.getElementById('fullName');
var phone = document.getElementById('phone');
var comment = document.getElementById('comment');
var preloader = document.querySelector('.preloader');

var checkedDelivery;
var total = document.querySelector('form p.total');

var deliveryData;

// присваиваем полю с именем currency CharCode для передачи на страницу поиска и сортировки
document.getElementsByName('currency')[0].value = currentCharCode;

// Вывод инфо о книге
showElementBook(JSON.parse(sessionStorage.bookDetails));

function showElementBook(obj) {
    var cover = document.querySelector('.col img');
    cover.src = obj.cover.small;

    var name = document.createElement('a');
    var title = document.querySelector('main h2');
    title.innerHTML += '<a href="book.html?id=' + id + '">' + obj.title + '</a>';
}

// Вывод способов доставки
var xhr1 = new XMLHttpRequest();
xhr1.open('GET', 'http://netology-fbb-store-api.herokuapp.com/order/delivery', true);
xhr1.send();  // 2 аргумент true необходим, чтобы способы доставки гарантировано были загружены ранее способов оплаты, т.к. в противном случае checkedDelivery = undefined (54 строка)

xhr1.onload = function () {
    deliveryData = JSON.parse(xhr1.responseText);

    if(xhr1.status === 200) {
      showOrderDetail(deliveryData);
    }
};

function showOrderDetail(obj) {

    for(var i = 0; i < obj.length; i ++) {
        var item = document.createElement('input');
        item.id = obj[i].id;
        item.dataset.currency = obj[i].currency;
        item.dataset.price = obj[i].price;  // чтобы добавить к цене книги
        item.name = 'delivery';
        item.type = 'radio';
        if(obj[i].name == "Почтой России") {
          item.checked = 'true';
          checkedDelivery = item; // сохраняем начальный способ доставки, чтобы можно было вызвать на нем событие change
        }
        var tmp = obj[i].price == 0 ? 'бесплатно' : obj[i].price + ' ' + currentCharCode;
        var textItem =  document.createTextNode(' ' + obj[i].name + ' - ' + tmp);
        var br = document.createElement('br');

        delivery.appendChild(item);
        delivery.appendChild(textItem);
        delivery.appendChild(br);
    }
}

// Вывод способов оплаты
var xhr2 = new XMLHttpRequest();
xhr2.open('GET', 'http://netology-fbb-store-api.herokuapp.com/order/payment');
xhr2.send();

xhr2.onload = function () {
    var res = JSON.parse(xhr2.responseText);

    if(xhr2.status === 200) {
        showPaymentWay(res);
        // генерация встроенного события change на начальном способе доставки для начального определения способов оплаты и наличия поля Адрес
        var event = new Event('change', {bubbles: true}); // второй параметр {bubbles: true} нужен, чтобы была возможность словить событие на родительском элементе delivery
        checkedDelivery.dispatchEvent(event);
    }
};

function showPaymentWay(obj) {

    for(var i = 0; i < obj.length; i ++) {
        var item = document.createElement('input');
        item.id = obj[i].id;

        item.name = 'payment';
        item.type = 'radio';
        // if(obj[i].title == "Наложенный платеж") {item.checked = 'true';} // уже нет необходимости при 86 строке
        var textItem =  document.createTextNode(' ' + obj[i].title);
        var br = document.createElement('br');

        payment.appendChild(item);
        payment.appendChild(textItem);
        payment.appendChild(br);
    }
}

// Доступные способы оплаты

delivery.addEventListener('change', choosePayment);

function choosePayment(event) {
    checkedDelivery = event.target;
    var deliveryId = checkedDelivery.getAttribute('id');

    if(event.target.getAttribute('name') == 'delivery') {
          var xhr = new XMLHttpRequest();
          xhr.open('GET', 'http://netology-fbb-store-api.herokuapp.com/order/delivery/'
            + deliveryId + '/payment'); // массив с доступными методами оплаты для этого вида доставки
          xhr.send();

          xhr.onload = function () {
            var res = JSON.parse(xhr.responseText);

            if(xhr.status === 200) {
                checkAvailablePayment(res, deliveryId);
            }
        };
    }
}

function checkAvailablePayment(obj, deliveryId) {
    // Удаление поля адрес (если необходимо)
    if(document.getElementById('adress')) {
        payment.removeChild(document.getElementById('adress'));
        payment.removeChild(document.getElementById('adressLabel'));
        payment.removeChild(document.getElementById('brId'));
    }
    // Вывод поля адрес (если необходимо)
    for(var i = 0; i < deliveryData.length; i++) {
        if(deliveryData[i].id == deliveryId && deliveryData[i].needAdress) {
            var adress = document.createElement('input');
            adress.type = 'text';
            adress.id = 'adress';
            adress.placeholder = 'Адрес';
            adress.value = 'New York City';  // Для целей проверки
            var adressLabel = document.createElement('label');
            adressLabel.htmlFor = 'adress';
            adressLabel.id = 'adressLabel';
            adressLabel.textContent = 'Адрес';
            var br = document.createElement('br');
            br.id = 'brId';

            payment.insertBefore(adress, payment.children[0]);
            payment.insertBefore(adressLabel, adress);
            payment.insertBefore(br, adress);
        }
    }

    var inputPayment = document.querySelectorAll('input[name="payment"]');
    for(var i = 0; i < inputPayment.length; i++) {
        for(var j = 0; j < obj.length; j++) {
            if(inputPayment[i].getAttribute('id') == obj[j].id) {
                inputPayment[i].disabled = ''; // inputPayment[i].disabled = 'false' - в результате устанавливает элемент в disabled. Снимает disabled только путое значение
                break;
            }
            inputPayment[i].disabled = 'true';
        }
    }
    // Установка Предварительно активированного переключателя
    for(i = 0; i < inputPayment.length; i++) {
        if(inputPayment[i].disabled == '') {
            inputPayment[i].checked = 'true';
            break;
        }
    }
    // Установка цены в поле total (Итого к оплате)
    var arr = JSON.parse(sessionStorage.books);
    for(var i = 0; i < arr.length; i++) {
        if(arr[i].id == id) {
            var price = arr[i].price;
        }
    }
    var tmp = +checkedDelivery.dataset.price + price;
    total.textContent = 'Итого к оплате: ' + tmp + ' ' + currentCharCode;
}


// Отправка заказа на сервер
sendOrderButton.onclick = function () {
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://netology-fbb-store-api.herokuapp.com/order');
      //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.setRequestHeader('Content-Type', 'application/json');

      xhr.onloadstart = function () {
        sendOrderButton.style.display = 'none';
        preloader.style.display = 'block';
      };
      xhr.onloadend = function () {
        sendOrderButton.style.display = 'block';
        preloader.style.display = 'none';
      };


      xhr.addEventListener('load', function () {
        var res = JSON.parse(xhr.responseText);

        if(xhr.status === 200) {

        }
      });
      // В формате Querystring
      /*var body = 'manager=' + encodeURIComponent('test@gmail.com') + '&book=' + id
       + '&name=' + encodeURIComponent(fullName.value) + '&phone=' + encodeURIComponent(phone.value)
       + '&email=' + encodeURIComponent(email.value) + '&comment=' + encodeURIComponent(comment.value)
       + '&deliveryId=' + encodeURIComponent('delivery-01') + '&deliveryAddress=' + encodeURIComponent('New York City')
       + '&paymentId=' + encodeURIComponent('payment-01') + '&paymentCurrency=' + encodeURIComponent('R01235');*/
      // В формате JSON
      var body = {
        'manager': 'sokoldp3@gmail.com',
        'book': id,
        'name': fullName.value,
        'phone': phone.value,
        'email': email.value,
        'comment': comment.value,
        delivery: {
          'id': loop('input[name="delivery"]', 'id'),
          'address': function () {
            if(document.getElementById('adress')) {
              return document.getElementById('adress').value;
            }
          }()
        },
        'payment': {
          'id': loop('input[name="payment"]', 'id'),
          'currency': function () {
            var inputDelivery = document.querySelectorAll('input[name="delivery"]');
            for(var i = 0; i < inputDelivery.length; i++) {
              if(inputDelivery[i].checked) {
                return inputDelivery[i].dataset.currency;
              }
            }
          }()
        }
      };
      body = JSON.stringify(body);
      xhr.send(body);

      function loop(selector, field) {
        var nodeList = document.querySelectorAll(selector);
        for(var i = 0; i < nodeList.length; i++) {
          if(nodeList[i].checked) {
            return nodeList[i][field];
          }
        }
      }

      return false;
};