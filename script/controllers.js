'use strict';

searchApp.controller('BooksLIstCtrl', function ($scope, $http) {
    $scope.books = JSON.parse(sessionStorage.books);

    var index = location.href.indexOf('search=');
    $scope.query = decodeURI(location.href.slice(index + 7));
    index = location.href.indexOf('currency=');
    $scope.charCode = decodeURI(location.href.slice(index + 9, index + 12));
});
